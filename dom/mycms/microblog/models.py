# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Wpis(models.Model):
    author = models.ForeignKey(User, related_name='author', unique=False)
    title = models.CharField(max_length=40)
    text = models.TextField(u'Tekst wpisu')
    date = models.DateTimeField(u'Data wpisu')