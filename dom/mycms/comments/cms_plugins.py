# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import CommentPlugin, Comment
from forms import CommentForm


class CommentPlugin(CMSPluginBase):
    model = CommentPlugin  # model, który przechowuje dane o wtyczce
    name = _("Komentarz - wtyczka")  # nazwa wtyczki
    render_template = "comments/comment_plug.html" # szablon do renderowania pluginu
    #form = CommentForm

    def render(self, context, instance, placeholder):
        request = context['request']

        if request.method == 'POST':
            form = CommentForm(request.POST)
            if form.is_valid():
                form.save()

        context.update({
            'comment': instance,
            'placeholder': placeholder,
            'komentarze': Comment.objects.all().filter(page=request.current_page),
            'form' : CommentForm(initial={'page': request.current_page})
        })

        return context

plugin_pool.register_plugin(CommentPlugin) # rejestracja wtyczki