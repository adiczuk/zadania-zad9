# -*- coding: utf-8 -*-

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
#from .menu import PollsMenu
from django.utils.translation import ugettext_lazy as _


class ContactApp(CMSApp):
    name = _("Contact App")   # nazwa dołączonej aplikacji
    urls = ["contact.urls"]  # podłączenie urli aplikacji
    #menus = [PollsMenu]

apphook_pool.register(ContactApp) # rejestracja aplikacji