from django.forms import ModelForm, HiddenInput, Textarea, TextInput
from .models import Contact
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.db import models

class ContactForm(ModelForm):

    class Meta:
        model = Contact
        fields = [
            'author', 'title', 'text'
        ]