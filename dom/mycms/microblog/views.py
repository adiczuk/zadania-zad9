# -*- coding: utf-8 -*-
from .models import Wpis
from django.contrib.auth.models import User
from .forms import WpisForm, LoginForm, MyRegistrationForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import timezone

def index(request):
    info = request.session.get('info', None)
    if info:
        del request.session['info']
    wpisy = Wpis.objects.all().order_by('-pk')
    return render(request, 'microblog/main.html', {"wpisy" : wpisy, "info" : info})

def dodaj_wpis(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            form = WpisForm(initial={'author': request.user, 'date': timezone.now(), 'edited': None})
            return render(request, 'microblog/add.html', {"user_nick" : request.user.username, "form" : form})
        request.session['info'] = u'Najpierw się zaloguj!'
        return HttpResponseRedirect(reverse('login'))

    elif request.method == 'POST':
        form = WpisForm(request.POST)

        if form.is_valid():
            form.save()
            request.session['info'] = u'Dodano wpis!'
            return HttpResponseRedirect(reverse('blog'))
        return render(request, 'microblog/add.html', {"form" : form})

def zaloguj(request):
    info = request.session.get('info', None)

    if request.method == 'GET':
        if request.user.is_authenticated():
            request.session['info'] = u'Jesteś już zalogowany!'
            return HttpResponseRedirect(reverse('blog'))
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'microblog/login.html', {"info" : info, 'form' : form})

    elif request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])

        if user:
            login(request, user)
            request.session['info'] = u'Zostałeś pomyślnie zalogowany!'
            return HttpResponseRedirect(reverse('blog'))
        else:
            info = u'Złe dane!'
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'microblog/login.html', {"info" : info, 'form' : form})


def wyloguj(request):
    logout(request)
    request.session['info'] = u'Zostałeś wylogowany!'
    return HttpResponseRedirect(reverse('blog'))

def rejestracja(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            request.session['info'] = u'Twoje konto zostało zarejestrowane!'
            return HttpResponseRedirect(reverse('blog'))
    else:
        form = MyRegistrationForm()
    return render(request, "microblog/register.html", {'form': form})

def moje_wpisy(request):
    if request.user.is_authenticated():
        author = User.objects.get(username=request.user.username)
        wpisy = Wpis.objects.all().order_by('-pk').filter(author=author)
        return render(request, 'microblog/mine.html', {"wpisy" : wpisy})
    else:
        request.session['info'] = u'Najpierw się zaloguj!'
        return HttpResponseRedirect(reverse('login'))

def filtruj(request):
    users = User.objects.all().order_by('username')
    return render(request, 'microblog/users.html', {"users" : users})

def wpisy(request, pk):
    try:
        user = User.objects.get(pk=pk)
    except:
        return render(request, 'microblog/users_posts.html', {"wpisy" : None, 'user_post' : None})
    wpisy = Wpis.objects.all().filter(author_id=pk).order_by('-pk')

    return render(request, 'microblog/users_posts.html', {"wpisy" : wpisy, 'user_post' : user})

def miesiace(request):
    mies = [(u"Styczeń", 1), ("Luty", 2), ("Marzec", 3), (u"Kwiecień", 4),
            ("Maj", 5), ("Czerwiec",  6), ("Lipiec", 7), (u"Sierpień", 8),
            ("Wrzesien", 9), (u"Październik", 10), ("Listopad", 11), (u"Grudzień", 12)]

    return render(request, 'microblog/months.html', {"miesiace" : mies})



def miesiac(request, pk):
    wpisy = Wpis.objects.all().filter(date__month=pk).order_by('-pk')
    return render(request, 'microblog/month_posts.html', {"wpisy" : wpisy})
