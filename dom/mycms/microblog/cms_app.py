# -*- coding: utf-8 -*-

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class MicroblogApp(CMSApp):
    name = _("Microblog App")   # nazwa dołączonej aplikacji
    urls = ["microblog.urls"]  # podłączenie urli aplikacji

apphook_pool.register(MicroblogApp) # rejestracja aplikacji
