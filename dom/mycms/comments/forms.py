# -*- coding: utf-8 -*-
from django.forms import ModelForm, HiddenInput, Textarea, TextInput
from .models import Comment
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.db import models

class CommentForm(ModelForm):

    class Meta:
        model = Comment
        fields = [
            'page', 'author', 'text'
        ]
        widgets = {
            'page': HiddenInput(),
        }