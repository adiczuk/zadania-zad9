from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Like

class FacebookLikePlugin(CMSPluginBase):
    model = Like
    name = 'Social Facebook Like Button Plugin'
    render_template = 'cms_social_facebook/like.html'

    def render(self, context, instance, placeholder):
        context.update({
            'like': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(FacebookLikePlugin)