from django.conf.urls import patterns, url, include
from .views import *

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index, name='blog'),
    url(r'^add$', dodaj_wpis, name='add'),
    url(r'^login$', zaloguj, name='login'),
    url(r'^logout$', wyloguj, name='blog_logout'),
    #url(r'^register$', rejestracja, name='blog_register'),
    url(r'^mine$', moje_wpisy, name='mine'),
    url(r'^filtr$', filtruj, name='filtr'),
    url(r'^user/(?P<pk>\d+)$', wpisy, name='user'),
    url(r'^month$', miesiace, name='months'),
    url(r'^month/(?P<pk>\d+)$', miesiac, name='month'),
)