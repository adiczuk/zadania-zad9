from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from models import Comment
from forms import CommentForm

# Create your views here.

def coms(request):
    comments = Comment.objects.all().filter(page=request.current_page)
    form = CommentForm(initial={'page': request.current_page})
    return render(request, 'comments/comment.html', {"komentarze" : comments, "form" : form, "base" : True})

def add_comment(request):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
    return HttpResponseRedirect(reverse('coms'))
