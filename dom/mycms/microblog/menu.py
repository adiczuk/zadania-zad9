# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class UsersMenu(CMSAttachMenu):
    name = _("Users menu")
    cache = False

    def get_nodes(self, request):
        nodes = []

        for user in User.objects.all().order_by("username"):
            nodes.append(NavigationNode(user.username, "/blog/user/"+str(user.pk), user.pk))
        return nodes

menu_pool.register_menu(UsersMenu)

class MonthsMenu(CMSAttachMenu):
    name = _("Months menu")
    cashe = False

    def get_nodes(self, request):
        miesiace = ["Styczen", "Luty", "Marzec", "Kwiecien",
                    "Maj", "Czerwiec", "Lipiec", "Sierpien",
                    "Wrzesien", "Pazdziernik", "Listopad", "Grudzien"]
        nodes = []
        i = 1
        for miesiac in miesiace:
            nodes.append(NavigationNode(miesiac, "/blog/month/" + str(i), i))
            i = i + 1

        return nodes

menu_pool.register_menu(MonthsMenu)

class FiltrMenu(CMSAttachMenu):
    name = _("Filtr menu")
    cache = False

    def get_nodes(self, request):
        nodes = []
        nodes.append(NavigationNode(_(u'Użytkownicy'), reverse('filtr'), 1))
        nodes.append(NavigationNode(_(u'Miesiące'), reverse('months'), 2))
        return nodes

menu_pool.register_menu(FiltrMenu)