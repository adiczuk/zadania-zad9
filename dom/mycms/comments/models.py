from django.db import models
from cms.models import CMSPlugin
from django.utils.translation import ugettext_lazy as _


class Comment(models.Model):
    page = models.CharField(max_length=50)
    author = models.CharField(max_length=50)
    text = models.TextField()

class CommentPlugin(CMSPlugin):

    podpis = models.CharField(_("Opis"), max_length=50, help_text="Bedzie pokazany nad forumlarzem")

    width = models.PositiveSmallIntegerField(_("Liczba kolumn"), default=None, null=True,
        blank=True)

    height = models.PositiveSmallIntegerField(_("Liczba wierszy"), default=None, null=True,
        blank=True)

    def __unicode__(self):
        return "Kommentarz o nazwie (%s)" % (self.podpis)
