from django.db import models
from cms.models import CMSPlugin
from django.utils.translation import ugettext_lazy as _


class Contact(models.Model):
    author = models.CharField(_("Nick"),max_length=50)
    title = models.CharField(_("Tytul"), max_length=50)
    text = models.TextField(_("Tresc"), max_length=800)
