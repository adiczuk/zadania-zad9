from django.contrib import admin
from models import Wpis

class WpisAdmin(admin.ModelAdmin):
    list_display = ['author', 'title', 'text', 'date']
    fields = ['title', 'text']
    ordering = ['-date',]
    list_filter = ['date']

admin.site.register(Wpis, WpisAdmin)