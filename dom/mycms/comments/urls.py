from django.conf.urls import patterns, url, include
from .views import *

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', coms, name='coms'),
    url(r'add_comment$', add_comment, name='add_comment')
)