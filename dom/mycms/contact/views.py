# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from forms import ContactForm
from django.core.mail import send_mail

# Create your views here.

def send(request):
    if request.method == 'GET':
        form = ContactForm()
        return render(request, 'contact/contact.html', {"form" : form, "info" : request.session.get('info', None)})
    elif request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            send_mail(request.POST["title"], request.POST["text"], "blog@example.com", ["adamdjango91@gmail.com"])
            request.session['info'] = u'Mail został wysłany!'
    return HttpResponseRedirect(reverse('send'))


